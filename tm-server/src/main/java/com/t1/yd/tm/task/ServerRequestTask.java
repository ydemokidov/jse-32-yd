package com.t1.yd.tm.task;

import com.t1.yd.tm.api.service.IAuthService;
import com.t1.yd.tm.api.service.IUserService;
import com.t1.yd.tm.component.Server;
import com.t1.yd.tm.dto.request.AbstractRequest;
import com.t1.yd.tm.dto.request.AbstractUserRequest;
import com.t1.yd.tm.dto.request.user.UserLoginRequest;
import com.t1.yd.tm.dto.request.user.UserLogoutRequest;
import com.t1.yd.tm.dto.request.user.UserShowProfileRequest;
import com.t1.yd.tm.dto.response.AbstractResponse;
import com.t1.yd.tm.dto.response.ApplicationErrorResponse;
import com.t1.yd.tm.dto.response.user.UserLoginResponse;
import com.t1.yd.tm.dto.response.user.UserLogoutResponse;
import com.t1.yd.tm.dto.response.user.UserShowProfileResponse;
import com.t1.yd.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public final class ServerRequestTask extends AbstractServerSocketTask {

    @Nullable
    private AbstractRequest request;
    @Nullable
    private AbstractResponse response;

    public ServerRequestTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server, socket);
    }

    public ServerRequestTask(@NotNull final Server server, @NotNull final Socket socket, @Nullable final String userId) {
        super(server, socket, userId);
    }

    @Override
    @SneakyThrows
    public void run() {

        processInput();

        processUserId();

        processLogin();

        processProfile();

        processLogout();

        processOperation();

        processOutput();

    }

    private void processUserId() {
        if (!(request instanceof AbstractUserRequest)) return;
        @NotNull final AbstractUserRequest abstractUserRequest = (AbstractUserRequest) request;
        abstractUserRequest.setUserId(userId);
    }

    @SneakyThrows
    private void processInput() {
        @NotNull final InputStream inputStream = socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Object object = objectInputStream.readObject();
        request = (AbstractRequest) object;
    }

    @SneakyThrows
    private void processOutput() {
        @NotNull final OutputStream outputStream = socket.getOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket, userId));
    }

    private void processLogout() {
        if (response != null) return;
        if (!(request instanceof UserLogoutRequest)) return;
        userId = null;
        response = new UserLogoutResponse();
    }

    private void processLogin() {
        if (response != null) return;
        if (!(request instanceof UserLoginRequest)) return;
        try {
            @NotNull final UserLoginRequest userLoginRequest = (UserLoginRequest) request;
            @NotNull final String login = userLoginRequest.getLogin();
            @NotNull final String password = userLoginRequest.getPassword();
            @NotNull final IAuthService authService = server.getBootstrap().getAuthService();
            @NotNull final User user = authService.check(login, password);
            userId = user.getId();
            UserLoginResponse userLoginResponse = new UserLoginResponse();
            userLoginResponse.setUser(user);
            response = userLoginResponse;
        } catch (@NotNull final Exception e) {
            response = new UserLoginResponse(e);
        }
    }

    private void processProfile() {
        if (response != null) return;
        if (!(request instanceof UserShowProfileRequest)) return;
        if (userId == null) {
            response = new UserShowProfileResponse();
            return;
        }
        @NotNull final IUserService userService = server.getBootstrap().getUserService();
        @Nullable final User user = userService.findOneById(userId);
        response = new UserShowProfileResponse(user);
    }

    private void processOperation() {
        if (response != null) return;
        try {
            @Nullable final Object result = server.call(request);
            response = (AbstractResponse) result;
        } catch (Exception e) {
            response = new ApplicationErrorResponse(e);
        }
    }

}
