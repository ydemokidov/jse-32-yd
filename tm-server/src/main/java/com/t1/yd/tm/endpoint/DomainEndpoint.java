package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.dto.request.data.*;
import com.t1.yd.tm.dto.response.data.*;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;

public class DomainEndpoint extends AbstractEndpoint implements IDataEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public DataBackupLoadResponse dataBackupLoad(@NotNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBackupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    public DataBackupSaveResponse dataBackupSave(@NotNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBackupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse dataBinaryLoad(@NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBinaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse dataBinarySave(@NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBinarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataBase64LoadResponse dataBase64Load(@NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBase64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse dataBase64Save(@NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBase64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataFasterXmlJsonSaveResponse dataFasterXmlJsonSave(@NotNull final DataFasterXmlJsonSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataFasterXmlJsonSave();
        return new DataFasterXmlJsonSaveResponse();
    }

    @NotNull
    @Override
    public DataFasterXmlJsonLoadResponse dataFasterXmlJsonLoad(@NotNull final DataFasterXmlJsonLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataFasterXmlJsonLoad();
        return new DataFasterXmlJsonLoadResponse();
    }

    @NotNull
    @Override
    public DataFasterXmlXmlSaveResponse dataFasterXmlXmlSave(@NotNull final DataFasterXmlXmlSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataFasterXmlXmlSave();
        return new DataFasterXmlXmlSaveResponse();
    }

    @NotNull
    @Override
    public DataFasterXmlXmlLoadResponse dataFasterXmlXmlLoad(@NotNull final DataFasterXmlXmlLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataFasterXmlXmlLoad();
        return new DataFasterXmlXmlLoadResponse();
    }

    @NotNull
    @Override
    public DataFasterXmlYmlSaveResponse dataFasterXmlYmlSave(@NotNull final DataFasterXmlYmlSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataFasterXmlYmlSave();
        return new DataFasterXmlYmlSaveResponse();
    }

    @NotNull
    @Override
    public DataFasterXmlYmlLoadResponse dataFasterXmlYmlLoad(@NotNull final DataFasterXmlYmlLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataFasterXmlYmlLoad();
        return new DataFasterXmlYmlLoadResponse();
    }

    @NotNull
    @Override
    public DataJaxbJsonLoadResponse dataJaxbJsonLoad(@NotNull final DataJaxbJsonLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJaxbJsonLoad();
        return new DataJaxbJsonLoadResponse();
    }

    @NotNull
    @Override
    public DataJaxbJsonSaveResponse dataJaxbJsonSave(@NotNull final DataJaxbJsonSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJaxbJsonSave();
        return new DataJaxbJsonSaveResponse();
    }

    @NotNull
    @Override
    public DataJaxbXmlLoadResponse dataJaxbXmlLoad(@NotNull final DataJaxbXmlLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJaxbXmlLoad();
        return new DataJaxbXmlLoadResponse();
    }

    @NotNull
    @Override
    public DataJaxbXmlSaveResponse dataJaxbXmlSave(@NotNull final DataJaxbXmlSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJaxbXmlSave();
        return new DataJaxbXmlSaveResponse();
    }
}
