package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.api.service.ISaltProvider;
import com.t1.yd.tm.api.service.IUserService;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.entity.UserNotFoundException;
import com.t1.yd.tm.exception.field.EmailEmptyException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.LoginEmptyException;
import com.t1.yd.tm.exception.field.PasswordEmptyException;
import com.t1.yd.tm.exception.user.IsEmailExistException;
import com.t1.yd.tm.exception.user.IsLoginExistException;
import com.t1.yd.tm.model.User;
import com.t1.yd.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final ISaltProvider saltProvider;

    public UserService(@NotNull final IUserRepository userRepository, @NotNull final IProjectRepository projectRepository, @NotNull final ITaskRepository taskRepository, @NotNull final ISaltProvider saltProvider) {
        super(userRepository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.saltProvider = saltProvider;
    }

    @NotNull
    @Override
    public User removeByLogin(@NotNull final String login) {
        @NotNull final User user = findByLogin(login);
        return remove(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        @NotNull final User user = findByEmail(email);
        return remove(user);
    }

    @NotNull
    public User remove(@NotNull final User userToRemove) {
        @Nullable final User removedUser = super.removeById(userToRemove.getId());
        if (removedUser == null) throw new UserNotFoundException();
        taskRepository.clear(removedUser.getId());
        projectRepository.clear(removedUser.getId());
        return removedUser;
    }

    @NotNull
    @Override
    public User setPassword(@NotNull final String id, @NotNull final String password) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        return user;
    }

    @NotNull
    @Override
    public User updateUser(@NotNull final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = repository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        if (firstName != null) user.setFirstName(firstName);
        if (lastName != null) user.setLastName(lastName);
        if (middleName != null) user.setMiddleName(middleName);
        return user;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new IsLoginExistException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        return repository.create(login, Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new IsLoginExistException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new IsEmailExistException();
        return repository.create(login, Objects.requireNonNull(HashUtil.salt(password, saltProvider)), email);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email, @Nullable Role role) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new IsLoginExistException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) role = Role.USUAL;
        return repository.create(login, Objects.requireNonNull(HashUtil.salt(password, saltProvider)), email, role);
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        @Nullable User user = repository.findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        return repository.isLoginExist(login);
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        return repository.isEmailExist(email);
    }

    @Override
    public void lockByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        user.setLocked(false);
    }

}