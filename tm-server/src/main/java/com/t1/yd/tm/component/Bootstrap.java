package com.t1.yd.tm.component;

import com.t1.yd.tm.api.endpoint.*;
import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.api.service.*;
import com.t1.yd.tm.dto.request.data.*;
import com.t1.yd.tm.dto.request.project.*;
import com.t1.yd.tm.dto.request.system.ServerAboutRequest;
import com.t1.yd.tm.dto.request.system.ServerVersionRequest;
import com.t1.yd.tm.dto.request.task.*;
import com.t1.yd.tm.dto.request.user.*;
import com.t1.yd.tm.endpoint.*;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;
import com.t1.yd.tm.repository.UserRepository;
import com.t1.yd.tm.service.*;
import com.t1.yd.tm.util.SystemUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "com.t1.yd.tm.command";

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();


    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IDataEndpoint dataEndpoint = new DomainEndpoint(this);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(UserLockRequest.class, userEndpoint::userLock);
        server.registry(UserPasswordChangeRequest.class, userEndpoint::userPasswordChange);
        server.registry(UserRegistryRequest.class, userEndpoint::userRegistry);
        server.registry(UserRemoveRequest.class, userEndpoint::userRemove);
        server.registry(UserUnlockRequest.class, userEndpoint::userUnlock);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::userUpdateProfile);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::projectChangeStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::projectChangeStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::projectClear);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::projectCompleteById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::projectCompleteByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::projectCreate);
        server.registry(ProjectListRequest.class, projectEndpoint::projectList);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::projectRemoveById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::projectRemoveByIndex);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::projectShowById);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::projectStartById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::projectStartByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::projectUpdateById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::projectUpdateByIndex);

        server.registry(TaskBindToProjectRequest.class, taskEndpoint::taskBindToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::taskChangeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::taskChangeStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::taskClear);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::taskCompleteById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::taskCompleteByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::taskCreate);
        server.registry(TaskListByProjectIdRequest.class, taskEndpoint::taskListByProjectId);
        server.registry(TaskListRequest.class, taskEndpoint::taskList);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::taskRemoveById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::taskRemoveByIndex);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::taskShowById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::taskShowByIndex);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::taskStartById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::taskStartByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::taskUnbindFromProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::taskUpdateById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::taskUpdateByIndex);

        server.registry(DataBackupSaveRequest.class, dataEndpoint::dataBackupSave);
        server.registry(DataBackupLoadRequest.class, dataEndpoint::dataBackupLoad);
        server.registry(DataBase64LoadRequest.class, dataEndpoint::dataBase64Load);
        server.registry(DataBase64SaveRequest.class, dataEndpoint::dataBase64Save);
        server.registry(DataBinaryLoadRequest.class, dataEndpoint::dataBinaryLoad);
        server.registry(DataBinarySaveRequest.class, dataEndpoint::dataBinarySave);
        server.registry(DataFasterXmlYmlLoadRequest.class, dataEndpoint::dataFasterXmlYmlLoad);
        server.registry(DataFasterXmlYmlSaveRequest.class, dataEndpoint::dataFasterXmlYmlSave);
        server.registry(DataFasterXmlXmlLoadRequest.class, dataEndpoint::dataFasterXmlXmlLoad);
        server.registry(DataFasterXmlXmlSaveRequest.class, dataEndpoint::dataFasterXmlXmlSave);
        server.registry(DataFasterXmlJsonLoadRequest.class, dataEndpoint::dataFasterXmlJsonLoad);
        server.registry(DataFasterXmlJsonSaveRequest.class, dataEndpoint::dataFasterXmlJsonSave);
        server.registry(DataJaxbXmlSaveRequest.class, dataEndpoint::dataJaxbXmlSave);
        server.registry(DataJaxbXmlLoadRequest.class, dataEndpoint::dataJaxbXmlLoad);
        server.registry(DataJaxbJsonSaveRequest.class, dataEndpoint::dataJaxbJsonSave);
        server.registry(DataJaxbJsonSaveRequest.class, dataEndpoint::dataJaxbJsonSave);
    }

    private void initBackup() {
        backup.init();
    }

    public void run(String[] args) {
        initDemoData();
        initLogger();
        initPID();
        initBackup();
        initServer();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void initDemoData() {
        userService.add(userService.create("admin", "admin", "admin@mail.ru", Role.ADMIN));
        userService.add(userService.create("user1", "user1", "user1@mail.ru"));
        userService.add(userService.create("user2", "user2", "user2@mail.ru"));

        String user1id = userService.findByLogin("user1").getId();
        String user2id = userService.findByLogin("user2").getId();

        projectRepository.add(user1id, new Project(user1id, "project1", "my first project"));
        projectRepository.add(user1id, new Project(user1id, "project2", "my 2nd project"));
        projectRepository.add(user1id, new Project(user1id, "project3", "my 3rd project"));
        projectRepository.add(user2id, new Project(user2id, "project4", "my 4th project"));
        projectRepository.add(user2id, new Project(user2id, "project5", "my 5th project"));

        taskRepository.add(user1id, new Task(user1id, "task1", "my 1st task"));
        taskRepository.add(user1id, new Task(user1id, "task2", "my 2nd task"));
        taskRepository.add(user2id, new Task(user2id, "task3", "my 3rd task"));
        taskRepository.add(user2id, new Task(user2id, "task4", "my 4th task"));

        projectTaskService.bindTaskToProject(user1id, taskRepository.findOneByIndex(0).getId(), projectRepository.findOneByIndex(0).getId());
        projectTaskService.bindTaskToProject(user1id, taskRepository.findOneByIndex(1).getId(), projectRepository.findOneByIndex(0).getId());
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }

    private void initServer() {
        server.start();
    }

    private void prepareShutdown() {
        backup.stop();
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }

    private void exit() {
        System.exit(0);
    }

}