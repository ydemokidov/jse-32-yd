package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.dto.request.project.*;
import com.t1.yd.tm.dto.response.project.*;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public ProjectChangeStatusByIdResponse projectChangeStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final String status = request.getStatus();
        @NotNull final Project project = getServiceLocator().getProjectService().changeStatusById(userId, id, Status.valueOf(status));
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    public ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final String status = request.getStatus();
        @NotNull final Project project = getServiceLocator().getProjectService().changeStatusByIndex(userId, index, Status.valueOf(status));
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @Override
    public ProjectClearResponse projectClear(@NotNull final ProjectClearRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        getServiceLocator().getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @Override
    public ProjectCompleteByIdResponse projectCompleteById(@NotNull final ProjectCompleteByIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final Project project = getServiceLocator().getProjectService().changeStatusById(userId, id, Status.COMPLETED);
        return new ProjectCompleteByIdResponse(project);
    }

    @Override
    public ProjectCompleteByIndexResponse projectCompleteByIndex(@NotNull final ProjectCompleteByIndexRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Project project = getServiceLocator().getProjectService().changeStatusByIndex(userId, index, Status.COMPLETED);
        return new ProjectCompleteByIndexResponse(project);
    }

    @Override
    public ProjectCreateResponse projectCreate(@NotNull final ProjectCreateRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Project project = getServiceLocator().getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    public ProjectListResponse projectList(@NotNull final ProjectListRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final List<Project> projects = getServiceLocator().getProjectService().findAll(userId);
        ProjectListResponse response = new ProjectListResponse();
        response.setProjects(projects);
        return response;
    }

    @Override
    public ProjectRemoveByIdResponse projectRemoveById(@NotNull final ProjectRemoveByIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final Project project = getServiceLocator().getProjectService().removeProjectById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @Override
    public ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Project project = getServiceLocator().getProjectService().removeProjectByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @Override
    public ProjectShowByIdResponse projectShowById(@NotNull final ProjectShowByIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final Project project = getServiceLocator().getProjectService().findProjectById(userId, id);
        return new ProjectShowByIdResponse(project);
    }

    @Override
    public ProjectShowByIndexResponse projectShowByIndex(@NotNull final ProjectShowByIndexRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Project project = getServiceLocator().getProjectService().findProjectByIndex(userId, index);
        return new ProjectShowByIndexResponse(project);
    }

    @Override
    public ProjectStartByIdResponse projectStartById(@NotNull final ProjectStartByIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final Project project = getServiceLocator().getProjectService().changeStatusById(userId, id, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse(project);
    }

    @Override
    public ProjectStartByIndexResponse projectStartByIndex(@NotNull final ProjectStartByIndexRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Project project = getServiceLocator().getProjectService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new ProjectStartByIndexResponse(project);
    }

    @Override
    public ProjectUpdateByIdResponse projectUpdateById(@NotNull final ProjectUpdateByIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Project project = getServiceLocator().getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @Override
    public ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Project project = getServiceLocator().getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

}