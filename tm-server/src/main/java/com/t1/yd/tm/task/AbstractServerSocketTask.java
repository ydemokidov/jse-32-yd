package com.t1.yd.tm.task;

import com.t1.yd.tm.component.Server;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    @Nullable
    protected String userId;

    public AbstractServerSocketTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server);
        this.socket = socket;
    }

    public AbstractServerSocketTask(@NotNull Server server, @NotNull Socket socket, @Nullable String userId) {
        super(server);
        this.socket = socket;
        this.userId = userId;
    }

}
