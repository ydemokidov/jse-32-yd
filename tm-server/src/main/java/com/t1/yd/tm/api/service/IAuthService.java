package com.t1.yd.tm.api.service;

import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email);

    User check(@Nullable String login, @Nullable String password);

}
