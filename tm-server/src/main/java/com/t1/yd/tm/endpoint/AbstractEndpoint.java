package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.api.service.IUserService;
import com.t1.yd.tm.dto.request.AbstractUserRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.user.AccessDeniedException;
import com.t1.yd.tm.model.User;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public class AbstractEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final IUserService userService = getServiceLocator().getUserService();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role userRole = user.getRole();
        final boolean check = userRole == role;
        if (!check) throw new AccessDeniedException();
    }

    protected void check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }


}
