package com.t1.yd.tm.task;

import com.t1.yd.tm.component.Server;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected final Server server;

}
