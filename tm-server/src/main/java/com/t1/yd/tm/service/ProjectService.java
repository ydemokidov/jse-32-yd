package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.service.IProjectService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.field.*;
import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Project findProjectById(@NotNull String id) {
        @Nullable Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    public Project findProjectByIndex(@NotNull String userId, @NotNull Integer index) {
        @Nullable Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    public Project findProjectById(@NotNull String userId, @NotNull String id) {
        @Nullable Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    public Project create(@NotNull String userId, @NotNull final String name, @NotNull final String description) {
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        return add(new Project(userId, name, description));
    }

    @NotNull
    @Override
    public Project updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();

        @NotNull final Project project = findProjectById(userId, id);

        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();

        @NotNull final Project project = findProjectByIndex(userId, index);

        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final Project project = findProjectById(userId, id);
        project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0 || index >= repository.getSize()) throw new IndexIncorrectException();
        @NotNull final Project project = findProjectByIndex(userId, index);
        project.setStatus(status);
        return project;
    }

    @NotNull
    public Project removeProjectById(@NotNull String userId, @NotNull String id) {
        @Nullable Project project = repository.removeById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    public Project removeProjectByIndex(@NotNull String userId, @NotNull Integer index) {
        @Nullable Project project = repository.removeByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

}