package com.t1.yd.tm.component;

import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.dto.request.AbstractRequest;
import com.t1.yd.tm.dto.response.AbstractResponse;
import com.t1.yd.tm.endpoint.Operation;
import com.t1.yd.tm.task.AbstractServerTask;
import com.t1.yd.tm.task.ServerAcceptTask;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Getter
public final class Server {

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();
    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();
    @NotNull
    private final Bootstrap bootstrap;
    @Nullable
    @Getter
    private ServerSocket serverSocket;

    public Server(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void submit(@NotNull final AbstractServerTask serverTask) {
        executorService.submit(serverTask);
    }

    @SneakyThrows
    public void start() {
        @NotNull final IPropertyService propertyService = bootstrap.getPropertyService();
        @NotNull final Integer port = propertyService.getServerPort();
        serverSocket = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(@NotNull final Class<RQ> reqClass, @NotNull final Operation<RQ, RS> operation) {
        dispatcher.registry(reqClass, operation);
    }

    public Object call(final AbstractRequest request) {
        return dispatcher.call(request);
    }

    @SneakyThrows
    public void stop() {
        if (serverSocket == null) return;
        serverSocket.close();
        executorService.shutdown();
    }

}
