package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IAuthService;
import com.t1.yd.tm.api.service.ISaltProvider;
import com.t1.yd.tm.api.service.IUserService;
import com.t1.yd.tm.exception.field.LoginEmptyException;
import com.t1.yd.tm.exception.field.PasswordEmptyException;
import com.t1.yd.tm.exception.user.IncorrectLoginOrPasswordException;
import com.t1.yd.tm.model.User;
import com.t1.yd.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ISaltProvider saltProvider;

    public AuthService(@NotNull final IUserService userService, @NotNull final ISaltProvider saltProvider) {
        this.userService = userService;
        this.saltProvider = saltProvider;
    }

    @NotNull
    @Override
    public User registry(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User check(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.getLocked()) throw new IncorrectLoginOrPasswordException();
        @Nullable final String hash = HashUtil.salt(password, saltProvider);
        if (hash == null) throw new IncorrectLoginOrPasswordException();
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        return user;
    }

}
