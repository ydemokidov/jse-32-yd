package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.dto.request.task.*;
import com.t1.yd.tm.dto.response.task.*;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @NotNull TaskBindToProjectResponse taskBindToProject(@NotNull final TaskBindToProjectRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final String projectId = request.getProjectId();
        getServiceLocator().getProjectTaskService().bindTaskToProject(userId, taskId, projectId);
        return new TaskBindToProjectResponse();
    }

    @Override
    public @NotNull TaskUnbindFromProjectResponse taskUnbindFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final String projectId = request.getProjectId();
        getServiceLocator().getProjectTaskService().unbindTaskFromProject(userId, taskId, projectId);
        return new TaskUnbindFromProjectResponse();
    }

    @Override
    public @NotNull TaskChangeStatusByIdResponse taskChangeStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final String status = request.getStatus();
        @NotNull final Task task = getServiceLocator().getTaskService().changeStatusById(userId, taskId, Status.valueOf(status));
        return new TaskChangeStatusByIdResponse(task);
    }

    @Override
    public @NotNull TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final String status = request.getStatus();
        @NotNull final Task task = getServiceLocator().getTaskService().changeStatusByIndex(userId, index, Status.valueOf(status));
        return new TaskChangeStatusByIndexResponse(task);
    }

    @Override
    public @NotNull TaskClearResponse taskClear(@NotNull final TaskClearRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        getServiceLocator().getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @Override
    public @NotNull TaskCompleteByIdResponse taskCompleteById(@NotNull final TaskCompleteByIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final Task task = getServiceLocator().getTaskService().changeStatusById(userId, taskId, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @Override
    public @NotNull TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull final TaskCompleteByIndexRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Task task = getServiceLocator().getTaskService().changeStatusByIndex(userId, index, Status.COMPLETED);
        return new TaskCompleteByIndexResponse(task);
    }

    @Override
    public @NotNull TaskCreateResponse taskCreate(@NotNull final TaskCreateRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Task task = getServiceLocator().getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @Override
    public @NotNull TaskListByProjectIdResponse taskListByProjectId(@NotNull final TaskListByProjectIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String projectId = request.getProjectId();
        @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findAllByProjectId(userId, projectId);
        return new TaskListByProjectIdResponse(tasks);
    }

    @Override
    public @NotNull TaskListResponse taskList(@NotNull final TaskListRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String sort = request.getSort();
        @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findAll(userId, Sort.valueOf(sort));
        return new TaskListResponse(tasks);
    }

    @Override
    public @NotNull TaskRemoveByIdResponse taskRemoveById(@NotNull final TaskRemoveByIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final Task task = getServiceLocator().getTaskService().removeTaskById(userId, taskId);
        return new TaskRemoveByIdResponse(task);
    }

    @Override
    public @NotNull TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Task task = getServiceLocator().getTaskService().removeTaskByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @Override
    public @NotNull TaskShowByIdResponse taskShowById(@NotNull final TaskShowByIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String taskId = request.getId();
        @Nullable final Task task = getServiceLocator().getTaskService().findTaskById(userId, taskId);
        return new TaskShowByIdResponse(task);
    }

    @Override
    public @NotNull TaskShowByIndexResponse taskShowByIndex(@NotNull final TaskShowByIndexRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @Nullable final Task task = getServiceLocator().getTaskService().findTaskByIndex(userId, index);
        return new TaskShowByIndexResponse(task);
    }

    @Override
    public @NotNull TaskStartByIdResponse taskStartById(@NotNull final TaskStartByIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final Task task = getServiceLocator().getTaskService().changeStatusById(userId, taskId, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @Override
    public @NotNull TaskStartByIndexResponse taskStartByIndex(@NotNull final TaskStartByIndexRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Task task = getServiceLocator().getTaskService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse(task);
    }

    @Override
    public @NotNull TaskUpdateByIdResponse taskUpdateById(@NotNull final TaskUpdateByIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Task task = getServiceLocator().getTaskService().updateById(userId, taskId, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @Override
    public @NotNull TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Task task = getServiceLocator().getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}
