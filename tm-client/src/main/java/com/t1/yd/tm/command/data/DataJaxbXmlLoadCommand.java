package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.request.data.DataJaxbXmlLoadRequest;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataJaxbXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "load_jaxb_xml";

    @NotNull
    private final String description = "Load data from XML with JAXB library";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[JAXB XML DATA LOAD]");
        DataJaxbXmlLoadRequest request = new DataJaxbXmlLoadRequest();
        getDataEndpointClient().dataJaxbXmlLoad(request);
        System.out.println("[DATA LOADED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
