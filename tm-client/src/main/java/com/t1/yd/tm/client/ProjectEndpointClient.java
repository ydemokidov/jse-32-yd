package com.t1.yd.tm.client;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.dto.request.project.*;
import com.t1.yd.tm.dto.request.user.UserLoginRequest;
import com.t1.yd.tm.dto.request.user.UserLogoutRequest;
import com.t1.yd.tm.dto.response.project.*;
import com.t1.yd.tm.dto.response.user.UserLoginResponse;
import com.t1.yd.tm.model.Project;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.stream.Collectors;

@NoArgsConstructor
public class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpoint {

    public ProjectEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(@NotNull String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        @NotNull final UserLoginResponse loginResponse = authEndpointClient.userLogin(new UserLoginRequest("user1", "user1"));
        System.out.println(loginResponse.isSuccess());

        @NotNull final ProjectEndpointClient projectEndpointClient = new ProjectEndpointClient(authEndpointClient);
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest("HELLO", "WORLD PROJECT");
        projectCreateRequest.setUserId(loginResponse.getUser().getId());
        System.out.println(projectEndpointClient.projectCreate(projectCreateRequest).getProject().getDescription());
        System.out.println(projectEndpointClient.projectList(new ProjectListRequest()).getProjects().stream().map(Project::toString).collect(Collectors.joining(";")));

        System.out.println(authEndpointClient.userLogout(new UserLogoutRequest()).isSuccess());
        authEndpointClient.disconnect();
    }

    @Override
    @SneakyThrows
    public ProjectChangeStatusByIdResponse projectChangeStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectClearResponse projectClear(@NotNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectCompleteByIdResponse projectCompleteById(@NotNull final ProjectCompleteByIdRequest request) {
        return call(request, ProjectCompleteByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectCompleteByIndexResponse projectCompleteByIndex(@NotNull final ProjectCompleteByIndexRequest request) {
        return call(request, ProjectCompleteByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectCreateResponse projectCreate(@NotNull final ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectListResponse projectList(@NotNull final ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectRemoveByIdResponse projectRemoveById(@NotNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectShowByIdResponse projectShowById(@NotNull final ProjectShowByIdRequest request) {
        return call(request, ProjectShowByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectShowByIndexResponse projectShowByIndex(@NotNull final ProjectShowByIndexRequest request) {
        return call(request, ProjectShowByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectStartByIdResponse projectStartById(@NotNull final ProjectStartByIdRequest request) {
        return call(request, ProjectStartByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectStartByIndexResponse projectStartByIndex(@NotNull final ProjectStartByIndexRequest request) {
        return call(request, ProjectStartByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectUpdateByIdResponse projectUpdateById(@NotNull final ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

}
