package com.t1.yd.tm.client;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.dto.request.data.*;
import com.t1.yd.tm.dto.response.data.*;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class DataEndpointClient extends AbstractEndpointClient implements IDataEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupLoadResponse dataBackupLoad(@NotNull final DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupSaveResponse dataBackupSave(@NotNull final DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinaryLoadResponse dataBinaryLoad(@NotNull final DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinarySaveResponse dataBinarySave(@NotNull final DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64LoadResponse dataBase64Load(@NotNull final DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64SaveResponse dataBase64Save(@NotNull final DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataFasterXmlJsonSaveResponse dataFasterXmlJsonSave(@NotNull final DataFasterXmlJsonSaveRequest request) {
        return call(request, DataFasterXmlJsonSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataFasterXmlJsonLoadResponse dataFasterXmlJsonLoad(@NotNull final DataFasterXmlJsonLoadRequest request) {
        return call(request, DataFasterXmlJsonLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataFasterXmlXmlSaveResponse dataFasterXmlXmlSave(@NotNull final DataFasterXmlXmlSaveRequest request) {
        return call(request, DataFasterXmlXmlSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataFasterXmlXmlLoadResponse dataFasterXmlXmlLoad(@NotNull final DataFasterXmlXmlLoadRequest request) {
        return call(request, DataFasterXmlXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataFasterXmlYmlSaveResponse dataFasterXmlYmlSave(@NotNull final DataFasterXmlYmlSaveRequest request) {
        return call(request, DataFasterXmlYmlSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataFasterXmlYmlLoadResponse dataFasterXmlYmlLoad(@NotNull final DataFasterXmlYmlLoadRequest request) {
        return call(request, DataFasterXmlYmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJaxbJsonLoadResponse dataJaxbJsonLoad(@NotNull final DataJaxbJsonLoadRequest request) {
        return call(request, DataJaxbJsonLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJaxbJsonSaveResponse dataJaxbJsonSave(@NotNull final DataJaxbJsonSaveRequest request) {
        return call(request, DataJaxbJsonSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJaxbXmlLoadResponse dataJaxbXmlLoad(@NotNull final DataJaxbXmlLoadRequest request) {
        return call(request, DataJaxbXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJaxbXmlSaveResponse dataJaxbXmlSave(@NotNull final DataJaxbXmlSaveRequest request) {
        return call(request, DataJaxbXmlSaveResponse.class);
    }

}
