package com.t1.yd.tm.command.user;

import com.t1.yd.tm.dto.request.user.UserLogoutRequest;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;

public class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private final String name = "user_logout";

    @NotNull
    private final String description = "User logout";

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        getUserEndpoint().userLogout(request);
        System.out.println("[USER LOGGED OUT]");
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
