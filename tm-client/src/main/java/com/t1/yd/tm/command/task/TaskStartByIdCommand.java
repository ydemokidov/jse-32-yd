package com.t1.yd.tm.command.task;

import com.t1.yd.tm.dto.request.task.TaskStartByIdRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_start_by_id";

    @NotNull
    public static final String DESCRIPTION = "Start task by Id";

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest();
        request.setId(id);
        getTaskEndpointClient().taskStartById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
