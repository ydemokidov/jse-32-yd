package com.t1.yd.tm.command.task;

import com.t1.yd.tm.dto.request.task.TaskUpdateByIdRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_update_by_id";

    @NotNull
    public static final String DESCRIPTION = "Update task by Id";

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");

        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String desc = TerminalUtil.nextLine();

        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest();
        request.setId(id);
        request.setName(name);
        request.setDescription(desc);
        getTaskEndpointClient().taskUpdateById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
