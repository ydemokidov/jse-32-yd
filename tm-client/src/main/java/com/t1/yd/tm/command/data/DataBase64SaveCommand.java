package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.request.data.DataBase64SaveRequest;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "save_base64";

    @NotNull
    private final String description = "Save data to file as Base64 String";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[BASE64 SAVE DATA]");
        DataBase64SaveRequest request = new DataBase64SaveRequest();
        getDataEndpointClient().dataBase64Save(request);
        System.out.println("[DATA SAVED]");
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
