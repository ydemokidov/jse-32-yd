package com.t1.yd.tm.command.user;

import com.t1.yd.tm.dto.request.user.UserShowProfileRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.NotNull;

public class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String name = "user_show_profile";

    @NotNull
    private final String description = "Show user profile";

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER PROFILE:]");

        @NotNull final UserShowProfileRequest request = new UserShowProfileRequest();

        @NotNull final User user = getUserEndpoint().userShowProfile(request).getUser();

        showUser(user);
        System.out.println("Last Name: " + user.getLastName());
        System.out.println("First Name: " + user.getFirstName());
        System.out.println("Middle Name: " + user.getMiddleName());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
