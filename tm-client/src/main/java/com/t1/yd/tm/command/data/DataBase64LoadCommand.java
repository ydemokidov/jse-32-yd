package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.request.data.DataBase64LoadRequest;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "load_base64";

    @NotNull
    private final String description = "Load data from base64 file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[BASE64 DATA LOAD]");
        DataBase64LoadRequest request = new DataBase64LoadRequest();
        getDataEndpointClient().dataBase64Load(request);
        System.out.println("[DATA LOADED]");
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }
}
