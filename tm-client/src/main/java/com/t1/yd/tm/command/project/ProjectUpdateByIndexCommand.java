package com.t1.yd.tm.command.project;

import com.t1.yd.tm.dto.request.project.ProjectUpdateByIndexRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project_update_by_index";

    @NotNull
    public static final String DESCRIPTION = "Update project by Index";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");

        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();

        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String desc = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest();
        request.setIndex(index);
        request.setName(name);
        request.setDescription(desc);
        getProjectEndpointClient().projectUpdateByIndex(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
