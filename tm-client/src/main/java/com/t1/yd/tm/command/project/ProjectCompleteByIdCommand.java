package com.t1.yd.tm.command.project;

import com.t1.yd.tm.dto.request.project.ProjectCompleteByIdRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project_complete_by_id";

    @NotNull
    public static final String DESCRIPTION = "Complete project by Id";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest();
        request.setId(id);
        getProjectEndpointClient().projectCompleteById(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
