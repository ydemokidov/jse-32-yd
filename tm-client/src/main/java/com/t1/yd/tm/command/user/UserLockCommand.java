package com.t1.yd.tm.command.user;

import com.t1.yd.tm.dto.request.user.UserLockRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public class UserLockCommand extends AbstractUserCommand {

    @NotNull
    private final String name = "user_lock";

    @NotNull
    private final String description = "User lock";

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest();
        request.setLogin(login);
        getUserEndpoint().userLock(request);
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
