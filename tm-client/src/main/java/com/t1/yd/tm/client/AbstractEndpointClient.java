package com.t1.yd.tm.client;

import com.t1.yd.tm.api.endpoint.IEndpointClient;
import com.t1.yd.tm.dto.response.ApplicationErrorResponse;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    protected String host = "localhost";

    @NotNull
    protected Integer port = 6060;

    @Nullable
    protected Socket socket;

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    public AbstractEndpointClient() {
    }

    protected <T> T call(@Nullable final Object data, @NotNull final Class<T> clazz) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getThrowable().getMessage());
        }
        return (T) result;
    }

    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    @Override
    public Socket connect() throws IOException {
        return new Socket(host, port);
    }

    @Override
    public void disconnect() throws IOException {
        if (socket == null) return;
        socket.close();
    }

}
