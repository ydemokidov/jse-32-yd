package com.t1.yd.tm.client;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.dto.request.project.ProjectCreateRequest;
import com.t1.yd.tm.dto.request.project.ProjectListRequest;
import com.t1.yd.tm.dto.request.task.*;
import com.t1.yd.tm.dto.request.user.UserLoginRequest;
import com.t1.yd.tm.dto.request.user.UserLogoutRequest;
import com.t1.yd.tm.dto.response.task.*;
import com.t1.yd.tm.dto.response.user.UserLoginResponse;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.model.Task;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.stream.Collectors;

@NoArgsConstructor
public class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpoint {

    public TaskEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(@NotNull final String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        @NotNull final UserLoginResponse loginResponse = authEndpointClient.userLogin(new UserLoginRequest("user1", "user1"));
        System.out.println(loginResponse.isSuccess());

        @NotNull final ProjectEndpointClient projectEndpointClient = new ProjectEndpointClient(authEndpointClient);
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest("HELLO", "WORLD PROJECT");
        projectCreateRequest.setUserId(loginResponse.getUser().getId());
        @NotNull final Project projectToCreate = projectEndpointClient.projectCreate(projectCreateRequest).getProject();
        System.out.println(projectToCreate.toString());
        System.out.println(projectEndpointClient.projectList(new ProjectListRequest()).getProjects().stream().map(Project::toString).collect(Collectors.joining(";")));

        @NotNull final TaskEndpointClient taskEndpointClient = new TaskEndpointClient(authEndpointClient);

        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest();
        taskCreateRequest.setName("CreatedTask");
        taskCreateRequest.setDescription("Task was created from endpoint");

        @NotNull final Task newTask = taskEndpointClient.taskCreate(taskCreateRequest).getTask();
        @NotNull final TaskBindToProjectRequest taskBindToProjectRequest = new TaskBindToProjectRequest();
        taskBindToProjectRequest.setProjectId(projectToCreate.getId());
        taskBindToProjectRequest.setId(newTask.getId());

        System.out.println(taskEndpointClient.taskBindToProject(taskBindToProjectRequest).isSuccess());

        System.out.println(authEndpointClient.userLogout(new UserLogoutRequest()).isSuccess());
        authEndpointClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse taskBindToProject(@NotNull final TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindFromProjectResponse taskUnbindFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIdResponse taskChangeStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskClearResponse taskClear(@NotNull final TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCompleteByIdResponse taskCompleteById(@NotNull final TaskCompleteByIdRequest request) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull final TaskCompleteByIndexRequest request) {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCreateResponse taskCreate(@NotNull final TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskListByProjectIdResponse taskListByProjectId(@NotNull final TaskListByProjectIdRequest request) {
        return call(request, TaskListByProjectIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskListResponse taskList(@NotNull final TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIdResponse taskRemoveById(@NotNull final TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIdResponse taskShowById(@NotNull final TaskShowByIdRequest request) {
        return call(request, TaskShowByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskShowByIndexResponse taskShowByIndex(@NotNull final TaskShowByIndexRequest request) {
        return call(request, TaskShowByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskStartByIdResponse taskStartById(@NotNull final TaskStartByIdRequest request) {
        return call(request, TaskStartByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskStartByIndexResponse taskStartByIndex(@NotNull final TaskStartByIndexRequest request) {
        return call(request, TaskStartByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIdResponse taskUpdateById(@NotNull final TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

}
