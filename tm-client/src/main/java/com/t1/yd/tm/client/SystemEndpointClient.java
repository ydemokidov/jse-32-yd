package com.t1.yd.tm.client;

import com.t1.yd.tm.api.endpoint.ISystemEndpoint;
import com.t1.yd.tm.dto.request.system.ServerAboutRequest;
import com.t1.yd.tm.dto.request.system.ServerVersionRequest;
import com.t1.yd.tm.dto.response.system.ServerAboutResponse;
import com.t1.yd.tm.dto.response.system.ServerVersionResponse;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    @SneakyThrows
    public static void main(@NotNull final String[] args) {
        @NotNull final SystemEndpointClient systemEndpointClient = new SystemEndpointClient();
        systemEndpointClient.connect();

        @NotNull final ServerAboutResponse serverAboutResponse = systemEndpointClient.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());

        @NotNull final ServerVersionResponse serverVersionResponse = systemEndpointClient.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());

        systemEndpointClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}
