package com.t1.yd.tm.command.project;

import com.t1.yd.tm.dto.request.project.ProjectShowByIdRequest;
import com.t1.yd.tm.dto.response.project.ProjectShowByIdResponse;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project_show_by_id";

    @NotNull
    public static final String DESCRIPTION = "Show project by Id";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");

        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest();
        request.setId(id);
        @NotNull final ProjectShowByIdResponse response = getProjectEndpointClient().projectShowById(request);
        @NotNull final Project project = response.getProject();

        showProject(project);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
