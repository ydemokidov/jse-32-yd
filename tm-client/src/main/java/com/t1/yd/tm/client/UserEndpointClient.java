package com.t1.yd.tm.client;

import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.dto.request.user.*;
import com.t1.yd.tm.dto.response.user.*;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public class UserEndpointClient extends AbstractEndpointClient implements IUserEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public UserPasswordChangeResponse userPasswordChange(@NotNull UserPasswordChangeRequest request) {
        return call(request, UserPasswordChangeResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse userLock(@NotNull UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @SneakyThrows
    public UserLoginResponse userLogin(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @SneakyThrows
    public UserLogoutResponse userLogout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRegistryResponse userRegistry(@NotNull UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRemoveResponse userRemove(@NotNull UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse userUnlock(@NotNull UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateProfileResponse userUpdateProfile(@NotNull UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @NotNull
    @SneakyThrows
    public UserShowProfileResponse userShowProfile(@NotNull UserShowProfileRequest request) {
        return call(request, UserShowProfileResponse.class);
    }

}