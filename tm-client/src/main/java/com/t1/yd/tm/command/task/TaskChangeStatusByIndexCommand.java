package com.t1.yd.tm.command.task;

import com.t1.yd.tm.dto.request.task.TaskChangeStatusByIndexRequest;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Objects;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_change_status_by_index";

    @NotNull
    public static final String DESCRIPTION = "Change task status by Index";

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Objects.requireNonNull(Status.toStatus(statusValue));
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest();
        request.setIndex(index);
        request.setStatus(status.toString());
        getTaskEndpointClient().taskChangeStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
