package com.t1.yd.tm.command.project;

import com.t1.yd.tm.dto.request.project.ProjectCreateRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project_create";

    @NotNull
    public static final String DESCRIPTION = "Create project";

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");

        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();

        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(name, description);
        getProjectEndpointClient().projectCreate(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
