package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.request.data.DataBackupSaveRequest;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "save_backup";

    @NotNull
    public final String description = "Save backup to file";

    @Override
    @SneakyThrows
    public void execute() {
        DataBackupSaveRequest request = new DataBackupSaveRequest();
        getDataEndpointClient().dataBackupSave(request);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
