package com.t1.yd.tm.command.server;

import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DisconnectCommand extends AbstractCommand {

    @NotNull
    public final static String NAME = "disconnect";

    @NotNull
    private final static String DESCRIPTION = "Disconnect from server";

    @Override
    public void execute() {
        try {
            getServiceLocator().getConnectionEndpointClient().disconnect();
            System.out.println("[DISCONNECTED]");
        } catch (Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }
}
