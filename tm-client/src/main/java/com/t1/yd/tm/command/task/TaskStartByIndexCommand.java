package com.t1.yd.tm.command.task;

import com.t1.yd.tm.dto.request.task.TaskStartByIndexRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_start_by_index";

    @NotNull
    public static final String DESCRIPTION = "Start task by Index";

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull TaskStartByIndexRequest request = new TaskStartByIndexRequest();
        request.setIndex(index);
        getTaskEndpointClient().taskStartByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
