package com.t1.yd.tm.command.data;

import com.t1.yd.tm.client.DataEndpointClient;
import com.t1.yd.tm.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected DataEndpointClient getDataEndpointClient() {
        return serviceLocator.getDataEndpointClient();
    }

}
