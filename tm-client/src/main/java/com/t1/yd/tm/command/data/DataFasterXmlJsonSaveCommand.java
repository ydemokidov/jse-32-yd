package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.request.data.DataFasterXmlJsonSaveRequest;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataFasterXmlJsonSaveCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "save_fasterxml_json";

    @NotNull
    private final String description = "Save json with FasterXML library";

    @Override
    public void execute() {
        System.out.println("[DATA SAVE FASTERXML]");
        DataFasterXmlJsonSaveRequest request = new DataFasterXmlJsonSaveRequest();
        getDataEndpointClient().dataFasterXmlJsonSave(request);
        System.out.println("[DATA SAVED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
