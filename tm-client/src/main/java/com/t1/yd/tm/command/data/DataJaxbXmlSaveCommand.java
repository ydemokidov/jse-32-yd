package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.request.data.DataJaxbXmlSaveRequest;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataJaxbXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "save_jaxb_xml";

    @NotNull
    private final String description = "Save XML with Jaxb library";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML JAXB]");
        DataJaxbXmlSaveRequest request = new DataJaxbXmlSaveRequest();
        getDataEndpointClient().dataJaxbXmlSave(request);
        System.out.println("[DATA SAVED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
