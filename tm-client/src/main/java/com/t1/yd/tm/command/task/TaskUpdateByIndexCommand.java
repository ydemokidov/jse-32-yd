package com.t1.yd.tm.command.task;

import com.t1.yd.tm.dto.request.task.TaskUpdateByIndexRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_update_by_index";

    @NotNull
    public static final String DESCRIPTION = "Update task by Index";

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");

        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();

        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String desc = TerminalUtil.nextLine();

        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest();
        request.setIndex(index);
        request.setName(name);
        request.setDescription(desc);

        getTaskEndpointClient().taskUpdateByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
