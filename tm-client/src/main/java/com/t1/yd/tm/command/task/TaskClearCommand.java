package com.t1.yd.tm.command.task;

import com.t1.yd.tm.dto.request.task.TaskClearRequest;
import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_clear";

    @NotNull
    public static final String DESCRIPTION = "Clear tasks";

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @NotNull final TaskClearRequest request = new TaskClearRequest();
        getTaskEndpointClient().taskClear(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
