package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.request.data.DataFasterXmlYmlSaveRequest;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataFasterXmlYmlSaveCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "save_fasterxml_yml";

    @NotNull
    private final String description = "Save YAML with FasterXml library";

    @Override
    public void execute() {
        System.out.println("[DATA SAVE FASTERXML YML]");
        DataFasterXmlYmlSaveRequest request = new DataFasterXmlYmlSaveRequest();
        getDataEndpointClient().dataFasterXmlYmlSave(request);
        System.out.println("[DATA SAVED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
