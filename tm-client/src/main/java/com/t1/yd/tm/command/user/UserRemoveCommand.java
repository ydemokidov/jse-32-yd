package com.t1.yd.tm.command.user;

import com.t1.yd.tm.dto.request.user.UserRemoveRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.User;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    private final String name = "user_remove";

    @NotNull
    private final String description = "Remove user";

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();

        @NotNull UserRemoveRequest request = new UserRemoveRequest();
        request.setLogin(login);

        @NotNull final User removedUser = getUserEndpoint().userRemove(request).getUser();

        System.out.println("[USER " + removedUser.getLogin() + " REMOVED]");
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
