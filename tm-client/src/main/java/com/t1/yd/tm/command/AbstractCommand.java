package com.t1.yd.tm.command;

import com.t1.yd.tm.api.model.ICommand;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.enumerated.Role;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    public abstract void execute();

    @NotNull
    public abstract String getName();

    @Nullable
    public abstract String getArgument();

    @Override
    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public abstract String getDescription();

    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();

        @NotNull String result = "";

        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

}
