package com.t1.yd.tm.command.user;

import com.t1.yd.tm.dto.request.user.UserLoginRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    private final String name = "user_login";

    @NotNull
    private final String description = "User login";

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        getUserEndpoint().userLogin(request);
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
