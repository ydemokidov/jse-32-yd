package com.t1.yd.tm.client;

import com.t1.yd.tm.dto.request.user.UserLoginRequest;
import com.t1.yd.tm.dto.request.user.UserLogoutRequest;
import com.t1.yd.tm.dto.request.user.UserShowProfileRequest;
import com.t1.yd.tm.dto.response.user.UserLoginResponse;
import com.t1.yd.tm.dto.response.user.UserLogoutResponse;
import com.t1.yd.tm.dto.response.user.UserShowProfileResponse;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class AuthEndpointClient extends AbstractEndpointClient {

    public static void main(@NotNull final String[] args) throws IOException {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.userShowProfile(new UserShowProfileRequest()).getUser());
        @NotNull final UserLoginResponse loginResponse = authEndpointClient.userLogin(new UserLoginRequest("user1", "user1"));
        @NotNull final String userId = loginResponse.getUser().getId();

        @NotNull final UserShowProfileRequest showProfileRequest = new UserShowProfileRequest();
        showProfileRequest.setUserId(userId);

        System.out.println(authEndpointClient.userShowProfile(showProfileRequest).getUser());

        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest();
        logoutRequest.setUserId(userId);
        System.out.println(authEndpointClient.userLogout(logoutRequest).isSuccess());

        authEndpointClient.disconnect();
    }

    @NotNull
    @SneakyThrows
    public UserLoginResponse userLogin(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @SneakyThrows
    public UserLogoutResponse userLogout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @SneakyThrows
    public UserShowProfileResponse userShowProfile(@NotNull final UserShowProfileRequest request) {
        return call(request, UserShowProfileResponse.class);
    }

}
