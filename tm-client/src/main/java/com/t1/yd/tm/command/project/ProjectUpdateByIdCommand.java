package com.t1.yd.tm.command.project;

import com.t1.yd.tm.dto.request.project.ProjectUpdateByIdRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project_update_by_id";

    @NotNull
    public static final String DESCRIPTION = "Update project by Id";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");

        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String desc = TerminalUtil.nextLine();

        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest();
        request.setId(id);
        request.setName(name);
        request.setDescription(desc);
        getProjectEndpointClient().projectUpdateById(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
