package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.request.data.DataBackupLoadRequest;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "load_backup";

    @NotNull
    private final String description = "Load data from backup file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD BACKUP DATA]");
        @NotNull DataBackupLoadRequest request = new DataBackupLoadRequest();
        getDataEndpointClient().dataBackupLoad(request);
        System.out.println("[BACKUP DATA LOADED]");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
