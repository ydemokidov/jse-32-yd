package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.request.data.DataFasterXmlXmlSaveRequest;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataFasterXmlXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "save-fasterxml-xml";

    private final String description = "Save XML with FasterXml library";

    @Override
    public void execute() {
        System.out.println("[DATA SAVE FASTERXML XML]");
        DataFasterXmlXmlSaveRequest request = new DataFasterXmlXmlSaveRequest();
        getDataEndpointClient().dataFasterXmlXmlSave(request);
        System.out.println("[DATA LOADED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
