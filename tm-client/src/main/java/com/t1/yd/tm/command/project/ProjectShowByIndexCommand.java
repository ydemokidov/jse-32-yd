package com.t1.yd.tm.command.project;

import com.t1.yd.tm.dto.request.project.ProjectShowByIndexRequest;
import com.t1.yd.tm.dto.response.project.ProjectShowByIndexResponse;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project_show_by_index";

    @NotNull
    public static final String DESCRIPTION = "Show project by Index";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER INDEX:");

        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest();
        request.setIndex(index);

        @NotNull final ProjectShowByIndexResponse response = getProjectEndpointClient().projectShowByIndex(request);
        @NotNull final Project project = response.getProject();
        showProject(project);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
