package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.request.data.DataFasterXmlYmlLoadRequest;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataFasterXmlYmlLoadCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "load_fasterxml_yml";

    @NotNull
    private final String description = "Load YAML with FasterXml library";

    @Override
    public void execute() {
        System.out.println("[DATA LOAD FASTERXML YML]");
        DataFasterXmlYmlLoadRequest request = new DataFasterXmlYmlLoadRequest();
        getDataEndpointClient().dataFasterXmlYmlLoad(request);
        System.out.println("[DATA LOADED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
