package com.t1.yd.tm.command.server;

import com.t1.yd.tm.api.endpoint.IEndpointClient;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "connect";

    @NotNull
    private static final String DESCRIPTION = "Connect to server";

    @Override
    @SneakyThrows
    public void execute() {
        try {
            @NotNull final IServiceLocator serviceLocator = getServiceLocator();
            @NotNull final IEndpointClient endpointClient = serviceLocator.getConnectionEndpointClient();
            @Nullable final Socket socket = endpointClient.connect();
            serviceLocator.getAuthEndpointClient().setSocket(socket);
            serviceLocator.getDataEndpointClient().setSocket(socket);
            serviceLocator.getProjectEndpointClient().setSocket(socket);
            serviceLocator.getTaskEndpointClient().setSocket(socket);
            serviceLocator.getAuthEndpointClient().setSocket(socket);
            serviceLocator.getSystemEndpointClient().setSocket(socket);
            serviceLocator.getUserEndpointClient().setSocket(socket);
            System.out.println("[CONNECTED]");
        } catch (Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
