package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.request.project.*;
import com.t1.yd.tm.dto.response.project.*;
import org.jetbrains.annotations.NotNull;

public interface IProjectEndpoint {

    ProjectChangeStatusByIdResponse projectChangeStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    ProjectClearResponse projectClear(@NotNull ProjectClearRequest request);

    ProjectCompleteByIdResponse projectCompleteById(@NotNull ProjectCompleteByIdRequest request);

    ProjectCompleteByIndexResponse projectCompleteByIndex(@NotNull ProjectCompleteByIndexRequest request);

    ProjectCreateResponse projectCreate(@NotNull ProjectCreateRequest request);

    ProjectListResponse projectList(@NotNull ProjectListRequest request);

    ProjectRemoveByIdResponse projectRemoveById(@NotNull ProjectRemoveByIdRequest request);

    ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull ProjectRemoveByIndexRequest request);

    ProjectShowByIdResponse projectShowById(@NotNull ProjectShowByIdRequest request);

    ProjectShowByIndexResponse projectShowByIndex(@NotNull ProjectShowByIndexRequest request);

    ProjectStartByIdResponse projectStartById(@NotNull ProjectStartByIdRequest request);

    ProjectStartByIndexResponse projectStartByIndex(@NotNull ProjectStartByIndexRequest request);

    ProjectUpdateByIdResponse projectUpdateById(@NotNull ProjectUpdateByIdRequest request);

    ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull ProjectUpdateByIndexRequest request);

}
