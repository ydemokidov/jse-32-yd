package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import com.t1.yd.tm.model.Project;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Getter
@Setter
public class ProjectListResponse extends AbstractResultResponse {

    private List<Project> projects;

    public ProjectListResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public ProjectListResponse() {
    }
}
