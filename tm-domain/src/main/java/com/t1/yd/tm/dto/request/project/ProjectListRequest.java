package com.t1.yd.tm.dto.request.project;

import com.t1.yd.tm.dto.request.AbstractUserRequest;
import com.t1.yd.tm.enumerated.Sort;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectListRequest extends AbstractUserRequest {

    private Sort sort;

}
