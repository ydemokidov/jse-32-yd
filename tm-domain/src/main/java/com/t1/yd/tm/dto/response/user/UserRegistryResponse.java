package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.Nullable;

public class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

    public UserRegistryResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
