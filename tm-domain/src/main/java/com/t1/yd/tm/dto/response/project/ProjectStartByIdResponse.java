package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.Nullable;

public class ProjectStartByIdResponse extends AbstractProjectResponse {

    public ProjectStartByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectStartByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
