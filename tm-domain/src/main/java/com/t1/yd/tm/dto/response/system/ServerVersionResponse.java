package com.t1.yd.tm.dto.response.system;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class ServerVersionResponse extends AbstractResultResponse {

    private String version;

    public ServerVersionResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public ServerVersionResponse() {
    }

}
