package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.Nullable;

public class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(@Nullable final User user) {
        super(user);
    }

    public UserUnlockResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
