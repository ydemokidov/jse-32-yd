package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.Nullable;

public class TaskChangeStatusByIndexResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIndexResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskChangeStatusByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
