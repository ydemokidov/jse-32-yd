package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.Nullable;

public class ProjectShowByIdResponse extends AbstractProjectResponse {

    public ProjectShowByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectShowByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
