package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.request.user.*;
import com.t1.yd.tm.dto.response.user.*;
import org.jetbrains.annotations.NotNull;

public interface IUserEndpoint {

    @NotNull
    UserPasswordChangeResponse userPasswordChange(@NotNull UserPasswordChangeRequest request);

    @NotNull
    UserLockResponse userLock(@NotNull UserLockRequest request);

    @NotNull
    UserUnlockResponse userUnlock(@NotNull UserUnlockRequest request);

    @NotNull
    UserRegistryResponse userRegistry(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse userRemove(@NotNull UserRemoveRequest request);

    @NotNull
    UserUpdateProfileResponse userUpdateProfile(@NotNull UserUpdateProfileRequest request);

}
