package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.request.system.ServerAboutRequest;
import com.t1.yd.tm.dto.request.system.ServerVersionRequest;
import com.t1.yd.tm.dto.response.system.ServerAboutResponse;
import com.t1.yd.tm.dto.response.system.ServerVersionResponse;
import org.jetbrains.annotations.NotNull;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
