package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.Nullable;

public class UserPasswordChangeResponse extends AbstractUserResponse {

    public UserPasswordChangeResponse(@Nullable final User user) {
        super(user);
    }

    public UserPasswordChangeResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
