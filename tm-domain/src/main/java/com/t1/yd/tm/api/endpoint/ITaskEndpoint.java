package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.request.task.*;
import com.t1.yd.tm.dto.response.task.*;
import org.jetbrains.annotations.NotNull;

public interface ITaskEndpoint {

    @NotNull
    TaskBindToProjectResponse taskBindToProject(@NotNull final TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindFromProjectResponse taskUnbindFromProject(@NotNull final TaskUnbindFromProjectRequest request);

    @NotNull
    TaskChangeStatusByIdResponse taskChangeStatusById(@NotNull final TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse taskClear(@NotNull final TaskClearRequest request);

    @NotNull
    TaskCompleteByIdResponse taskCompleteById(@NotNull final TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull final TaskCompleteByIndexRequest request);

    @NotNull
    TaskCreateResponse taskCreate(@NotNull final TaskCreateRequest request);

    @NotNull
    TaskListByProjectIdResponse taskListByProjectId(@NotNull final TaskListByProjectIdRequest request);

    @NotNull
    TaskListResponse taskList(@NotNull final TaskListRequest request);

    @NotNull
    TaskRemoveByIdResponse taskRemoveById(@NotNull final TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull final TaskRemoveByIndexRequest request);

    @NotNull
    TaskShowByIdResponse taskShowById(@NotNull final TaskShowByIdRequest request);

    @NotNull
    TaskShowByIndexResponse taskShowByIndex(@NotNull final TaskShowByIndexRequest request);

    @NotNull
    TaskStartByIdResponse taskStartById(@NotNull final TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse taskStartByIndex(@NotNull final TaskStartByIndexRequest request);

    @NotNull
    TaskUpdateByIdResponse taskUpdateById(@NotNull final TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull final TaskUpdateByIndexRequest request);

}