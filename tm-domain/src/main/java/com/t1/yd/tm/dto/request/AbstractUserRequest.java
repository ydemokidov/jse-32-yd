package com.t1.yd.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class AbstractUserRequest extends AbstractRequest {

    @Nullable
    private String userId;

}
