package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.Nullable;

public class ProjectCompleteByIndexResponse extends AbstractProjectResponse {

    public ProjectCompleteByIndexResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectCompleteByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
