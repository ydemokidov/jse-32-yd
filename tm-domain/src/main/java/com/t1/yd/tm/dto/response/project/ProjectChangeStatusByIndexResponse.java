package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.Nullable;

public class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIndexResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectChangeStatusByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
