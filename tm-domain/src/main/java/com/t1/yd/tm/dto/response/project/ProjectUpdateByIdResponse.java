package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.Nullable;

public class ProjectUpdateByIdResponse extends AbstractProjectResponse {

    public ProjectUpdateByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectUpdateByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
