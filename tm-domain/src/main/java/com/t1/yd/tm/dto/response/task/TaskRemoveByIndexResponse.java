package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.Nullable;

public class TaskRemoveByIndexResponse extends AbstractTaskResponse {

    public TaskRemoveByIndexResponse(Task task) {
        super(task);
    }

    public TaskRemoveByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
