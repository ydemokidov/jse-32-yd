package com.t1.yd.tm.dto.request.project;

import com.t1.yd.tm.dto.request.AbstractUserRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ProjectCreateRequest extends AbstractUserRequest {

    private String name;

    private String description;

}
