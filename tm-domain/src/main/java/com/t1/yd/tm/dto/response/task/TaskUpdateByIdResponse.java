package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse(@NotNull final Task task) {
        super(task);
    }

    public TaskUpdateByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
