package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskStartByIdResponse extends AbstractTaskResponse {

    public TaskStartByIdResponse(@NotNull final Task task) {
        super(task);
    }

    public TaskStartByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
