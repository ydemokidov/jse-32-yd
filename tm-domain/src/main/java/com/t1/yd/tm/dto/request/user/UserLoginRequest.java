package com.t1.yd.tm.dto.request.user;

import com.t1.yd.tm.dto.request.AbstractRequest;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@RequiredArgsConstructor
public class UserLoginRequest extends AbstractRequest {

    @NotNull
    private final String login;

    @NotNull
    private final String password;

}
