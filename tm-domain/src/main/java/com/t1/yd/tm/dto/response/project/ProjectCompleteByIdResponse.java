package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.Nullable;

public class ProjectCompleteByIdResponse extends AbstractProjectResponse {

    public ProjectCompleteByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectCompleteByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
