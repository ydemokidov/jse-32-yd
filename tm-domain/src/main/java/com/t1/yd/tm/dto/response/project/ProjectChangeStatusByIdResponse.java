package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.model.Project;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class ProjectChangeStatusByIdResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectChangeStatusByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
