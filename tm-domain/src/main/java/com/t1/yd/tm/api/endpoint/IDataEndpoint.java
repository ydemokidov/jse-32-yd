package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.request.data.*;
import com.t1.yd.tm.dto.response.data.*;
import org.jetbrains.annotations.NotNull;

public interface IDataEndpoint {

    @NotNull
    DataBackupLoadResponse dataBackupLoad(DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse dataBackupSave(DataBackupSaveRequest request);

    @NotNull
    DataBinaryLoadResponse dataBinaryLoad(DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse dataBinarySave(DataBinarySaveRequest request);

    @NotNull
    DataBase64LoadResponse dataBase64Load(DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse dataBase64Save(DataBase64SaveRequest request);

    @NotNull
    DataFasterXmlJsonSaveResponse dataFasterXmlJsonSave(DataFasterXmlJsonSaveRequest request);

    @NotNull
    DataFasterXmlJsonLoadResponse dataFasterXmlJsonLoad(DataFasterXmlJsonLoadRequest request);

    @NotNull
    DataFasterXmlXmlSaveResponse dataFasterXmlXmlSave(DataFasterXmlXmlSaveRequest request);

    @NotNull
    DataFasterXmlXmlLoadResponse dataFasterXmlXmlLoad(DataFasterXmlXmlLoadRequest request);

    @NotNull
    DataFasterXmlYmlSaveResponse dataFasterXmlYmlSave(DataFasterXmlYmlSaveRequest request);

    @NotNull
    DataFasterXmlYmlLoadResponse dataFasterXmlYmlLoad(DataFasterXmlYmlLoadRequest request);

    @NotNull
    DataJaxbJsonLoadResponse dataJaxbJsonLoad(DataJaxbJsonLoadRequest request);

    @NotNull
    DataJaxbJsonSaveResponse dataJaxbJsonSave(DataJaxbJsonSaveRequest request);

    @NotNull
    DataJaxbXmlLoadResponse dataJaxbXmlLoad(DataJaxbXmlLoadRequest request);

    @NotNull
    DataJaxbXmlSaveResponse dataJaxbXmlSave(DataJaxbXmlSaveRequest request);

}
