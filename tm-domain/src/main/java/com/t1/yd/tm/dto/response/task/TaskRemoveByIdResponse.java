package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskRemoveByIdResponse extends AbstractTaskResponse {

    public TaskRemoveByIdResponse(@NotNull final Task task) {
        super(task);
    }

    public TaskRemoveByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
