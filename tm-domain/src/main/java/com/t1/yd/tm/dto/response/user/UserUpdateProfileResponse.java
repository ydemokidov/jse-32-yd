package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.Nullable;

public class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(@Nullable final User user) {
        super(user);
    }

    public UserUpdateProfileResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
