package com.t1.yd.tm.dto.response.user;

import org.jetbrains.annotations.Nullable;

public class UserLoginResponse extends AbstractUserResponse {

    public UserLoginResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse() {
        super();
    }

}
