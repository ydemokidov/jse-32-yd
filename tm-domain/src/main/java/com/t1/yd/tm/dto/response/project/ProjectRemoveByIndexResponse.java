package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.Nullable;

public class ProjectRemoveByIndexResponse extends AbstractProjectResponse {

    public ProjectRemoveByIndexResponse(@Nullable Project project) {
        super(project);
    }

    public ProjectRemoveByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
