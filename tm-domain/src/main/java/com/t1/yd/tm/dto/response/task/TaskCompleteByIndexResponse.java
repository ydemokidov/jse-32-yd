package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.Nullable;

public class TaskCompleteByIndexResponse extends AbstractTaskResponse {

    public TaskCompleteByIndexResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskCompleteByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
