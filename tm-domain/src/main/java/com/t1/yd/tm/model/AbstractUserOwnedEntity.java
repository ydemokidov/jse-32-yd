package com.t1.yd.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public abstract class AbstractUserOwnedEntity extends AbstractEntity {

    @NotNull
    protected String userId;

    public AbstractUserOwnedEntity(@NotNull String userId) {
        this.userId = userId;
    }

}
