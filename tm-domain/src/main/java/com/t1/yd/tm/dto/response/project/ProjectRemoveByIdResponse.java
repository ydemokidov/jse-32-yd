package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.Nullable;

public class ProjectRemoveByIdResponse extends AbstractProjectResponse {

    public ProjectRemoveByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectRemoveByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
