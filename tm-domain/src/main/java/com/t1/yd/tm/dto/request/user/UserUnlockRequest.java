package com.t1.yd.tm.dto.request.user;

import com.t1.yd.tm.dto.request.AbstractUserRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class UserUnlockRequest extends AbstractUserRequest {

    @NotNull
    private String login;

}
