package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import com.t1.yd.tm.model.Task;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Getter
@Setter
public class TaskListByProjectIdResponse extends AbstractResultResponse {

    private List<Task> tasks;

    public TaskListByProjectIdResponse(@NotNull final List<Task> tasks) {
        this.tasks = tasks;
    }

    public TaskListByProjectIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
